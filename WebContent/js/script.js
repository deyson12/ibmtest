function abrirNuevaTarjeta() {
			$('#btnTrigger').click();
		}

		function abrirEditarTarjeta() {
			$('#btnTriggerEdit').click();
		}
		
		function abrirEditarClient() {
			$('#btnClientEdit').click();
		}
		
		function abrirEditarAsesor() {
			$('#btnAsesorEdit').click();
		}
		
		function openDeleteClient() {
			$('#btnClientDelete').click();
		}
		
		function openDeleteAsesor() {
			$('#btnAsesorDelete').click();
		}
		
		function openDeleteConsumption(){
			cerrar();
			$('#btnConsumptionDelete').click();
		}

		function cerrar() {
			$('[id^=product_view]').modal('hide');
			$("#newCard").modal('hide');
			$("#editCard").modal('hide');
			$('body').removeClass('modal-open');
			$('.modal-backdrop').remove();
		}