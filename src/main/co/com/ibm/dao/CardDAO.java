package main.co.com.ibm.dao;

import java.util.List;

import main.co.com.ibm.dto.Card;

public interface CardDAO {

	List<Card> cardListByClient(int idClient);

	int saveCard(Card card);

	int deleteCard(Card card);

	int editCard(Card cardEdit);

	Card getByCardNumber(String cardNumber);
	
}
