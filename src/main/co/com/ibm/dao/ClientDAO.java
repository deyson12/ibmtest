package main.co.com.ibm.dao;

import java.util.List;

import main.co.com.ibm.dto.Client;

public interface ClientDAO {

	public Client get(int i);

	public List<Client> list();

	public int saveClient(Client client);

	public int editClient(Client clientEdit);

	public int deleteClient(Client client);
	
}
