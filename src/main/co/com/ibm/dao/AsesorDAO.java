package main.co.com.ibm.dao;

import java.util.List;

import main.co.com.ibm.dto.Asesor;
import main.co.com.ibm.dto.Client;

public interface AsesorDAO {

	public Client get(int i);

	public List<Asesor> list();

	public int saveAsesor(Asesor asesor);

	public int editAsesor(Asesor asesorEdit);

	public int deleteAsesor(Asesor asesor);
	
}
