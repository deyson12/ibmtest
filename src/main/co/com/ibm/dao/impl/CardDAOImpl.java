package main.co.com.ibm.dao.impl;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;

import main.co.com.ibm.dao.CardDAO;
import main.co.com.ibm.dao.ConsumptionDAO;
import main.co.com.ibm.dto.Cache;
import main.co.com.ibm.dto.Card;
import main.co.com.ibm.utils.Conexion;

/**
 * Implementación para el guardado de Tarjetas en BD
 * @author deysespo
 *
 */
public class CardDAOImpl implements CardDAO, Serializable{

	private static final long serialVersionUID = 1L;
	private ConsumptionDAO consumptionDao;
	
	public CardDAOImpl() {
		consumptionDao = new ConsumptionDAOImpl();
	}
	
	/**
	 * Consulta de Tarjetas por Cliente
	 */
	@Override
	public List<Card> cardListByClient(int idClient) {
		List<Card> cardList = new ArrayList<>();
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("SELECT * FROM tblCard WHERE tblClient_idtblClient = ?");

			stmt.setInt(1, idClient);

			ResultSet rst = stmt.executeQuery();
			while (rst.next()) {
				Card card = new Card();
				card.setIdCard(rst.getInt("idtblCard"));
				card.setNumber(rst.getString("number"));
				card.setCcv(rst.getString("ccv"));
				card.setType(rst.getString("type"));
				card.setConsumptions(consumptionDao.consumptionListByCard(card.getIdCard()));
				cardList.add(card);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return cardList;
	}

	/**
	 * Guardar tarjeta
	 */
	@Override
	public int saveCard(Card card) {
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("INSERT INTO tblCard (number, ccv, type, tblClient_idtblClient) VALUES (?, ?, ?, ?);");
			
			stmt.setString(1, card.getNumber());
			stmt.setString(2, card.getCcv());
			stmt.setString(3, card.getType());
			stmt.setInt(4, card.getIdClient());
			
			int res = stmt.executeUpdate();
			if(res==1) {
				Cache.getInstance().getClients(true);
			}
			return res;
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Eliminar tarjetas
	 */
	@Override
	public int deleteCard(Card card) {
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("DELETE tblCard WHERE idtblCard = ?");
			
			stmt.setInt(1, card.getIdCard());
			
			int res = stmt.executeUpdate();
			if(res==1) {
				Cache.getInstance().getClients(true);
			}
			return res;
		
		}catch (JdbcSQLIntegrityConstraintViolationException e) {
			return -1;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Editar tarjeta
	 */
	@Override
	public int editCard(Card cardEdit) {
		
		try {
		PreparedStatement stmt = Conexion.getConexion()
				.prepareStatement("UPDATE tblCard SET number = ?, ccv = ?, type= ? WHERE idtblCard = ?");
		
		stmt.setString(1, cardEdit.getNumber());
		stmt.setString(2, cardEdit.getCcv());
		stmt.setString(3, cardEdit.getType());
		stmt.setInt(4, cardEdit.getIdCard());
		
		int res = stmt.executeUpdate();
		if(res==1) {
			Cache.getInstance().getClients(true);
		}
		return res;
		
		}catch (JdbcSQLIntegrityConstraintViolationException e) {
			return -1;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Obtener tarjeta por numero de tarjeta
	 */
	@Override
	public Card getByCardNumber(String cardNumber) {
		Card card = null;
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("SELECT * FROM tblCard WHERE number = ?");

			stmt.setString(1, cardNumber);

			ResultSet rst = stmt.executeQuery();
			if (rst.next()) {
				card = new Card();
				card.setIdCard(rst.getInt("idtblCard"));
				card.setNumber(rst.getString("number"));
				card.setCcv(rst.getString("ccv"));
				card.setType(rst.getString("type"));
				card.setConsumptions(consumptionDao.consumptionListByCard(card.getIdCard()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return card;
	}

}
