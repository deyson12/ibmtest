package main.co.com.ibm.dao.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;

import main.co.com.ibm.dao.CardDAO;
import main.co.com.ibm.dao.ClientDAO;
import main.co.com.ibm.dto.Cache;
import main.co.com.ibm.dto.Client;
import main.co.com.ibm.utils.Conexion;

/**
 * Implementación para el guardado de Clientes en BD
 * @author deysespo
 *
 */
public class ClientDAOImpl implements ClientDAO, Serializable{

	private static final long serialVersionUID = 1L;
	private CardDAO cardDao;
	
	public ClientDAOImpl() {
		cardDao = new CardDAOImpl();
	}

	/**
	 * Obtener cliente por ID
	 */
	public Client get(int idClient) {

		Client client = new Client();
		try {

			Connection con = Conexion.getConexion();

			PreparedStatement stmt = con.prepareStatement("SELECT * FROM tblClient WHERE idtblClient = ?");

			stmt.setInt(1, idClient);

			ResultSet rst = stmt.executeQuery();
			while (rst.next()) {
				client.setIdClient(rst.getInt("idtblClient"));
				client.setName(rst.getString("name"));
				client.setAddress(rst.getString("address"));
				client.setCity(rst.getString("city"));
				client.setPhone(rst.getString("phone"));
				client.setCards(cardDao.cardListByClient(client.getIdClient()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return client;
	}

	/**
	 * Listado de CLientes (Se guarda en Cache)
	 */
	@Override
	public List<Client> list() {

		List<Client> listaClientes = new ArrayList<>();
		try {

			PreparedStatement stmt = Conexion.getConexion().prepareStatement("SELECT * FROM tblClient");

			ResultSet rst = stmt.executeQuery();
			while (rst.next()) {
				Client client = new Client();
				client.setIdClient(rst.getInt("idtblClient"));
				client.setName(rst.getString("name"));
				client.setAddress(rst.getString("address"));
				client.setCity(rst.getString("city"));
				client.setPhone(rst.getString("phone"));
				client.setCards(cardDao.cardListByClient(client.getIdClient()));
				listaClientes.add(client);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return listaClientes;
	}

	/**
	 * Guardado de Cliente
	 */
	@Override
	public int saveClient(Client client) {
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("INSERT INTO tblClient (name, address, city, phone) VALUES (?, ?, ?, ?);");
			
			stmt.setString(1, client.getName());
			stmt.setString(2, client.getAddress());
			stmt.setString(3, client.getCity());
			stmt.setInt(4, Integer.parseInt(client.getPhone()));
			
			int res = stmt.executeUpdate();
			if(res==1) {
				Cache.getInstance().getClients(true);
			}
			return res;
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Editar cliente
	 */
	@Override
	public int editClient(Client clientEdit) {
		try {
			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("UPDATE tblClient SET name = ?, address = ?, city= ?, phone= ? WHERE idtblClient = ?");
			
			stmt.setString(1, clientEdit.getName());
			stmt.setString(2, clientEdit.getAddress());
			stmt.setString(3, clientEdit.getCity());
			stmt.setInt(4, Integer.parseInt(clientEdit.getPhone()));
			stmt.setInt(5, clientEdit.getIdClient());
			
			int res = stmt.executeUpdate();
			if(res==1) {
				Cache.getInstance().getClients(true);
			}
			return res;
			
		}catch (JdbcSQLIntegrityConstraintViolationException e) {
			return -1;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Delete cliente
	 */
	@Override
	public int deleteClient(Client client) {
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("DELETE tblClient WHERE idtblClient = ?");
			
			stmt.setInt(1, client.getIdClient());
			
			int res = stmt.executeUpdate();
			if(res==1) {
				Cache.getInstance().getClients(true);
			}
			return res;
		
		}catch (JdbcSQLIntegrityConstraintViolationException e) {
			return -1;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
