package main.co.com.ibm.dao.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;

import main.co.com.ibm.dao.AsesorDAO;
import main.co.com.ibm.dao.CardDAO;
import main.co.com.ibm.dao.ClientDAO;
import main.co.com.ibm.dto.Asesor;
import main.co.com.ibm.dto.Cache;
import main.co.com.ibm.dto.Client;
import main.co.com.ibm.utils.Conexion;

/**
 * Implementación para el guardado de Clientes en BD
 * @author deysespo
 *
 */
public class AsesorDAOImpl implements AsesorDAO, Serializable{

	private static final long serialVersionUID = 1L;
	private CardDAO cardDao;
	
	public AsesorDAOImpl() {
		cardDao = new CardDAOImpl();
	}

	/**
	 * Obtener cliente por ID
	 */
	public Client get(int idClient) {

		Client client = new Client();
		try {

			Connection con = Conexion.getConexion();

			PreparedStatement stmt = con.prepareStatement("SELECT * FROM tblClient WHERE idtblClient = ?");

			stmt.setInt(1, idClient);

			ResultSet rst = stmt.executeQuery();
			while (rst.next()) {
				client.setIdClient(rst.getInt("idtblClient"));
				client.setName(rst.getString("name"));
				client.setAddress(rst.getString("address"));
				client.setCity(rst.getString("city"));
				client.setPhone(rst.getString("phone"));
				client.setCards(cardDao.cardListByClient(client.getIdClient()));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return client;
	}

	/**
	 * Listado de CLientes (Se guarda en Cache)
	 */
	@Override
	public List<Asesor> list() {

		List<Asesor> listaAsesores = new ArrayList<>();
		try {

			PreparedStatement stmt = Conexion.getConexion().prepareStatement("SELECT * FROM tblBankingAdvisor");

			ResultSet rst = stmt.executeQuery();
			while (rst.next()) {
				Asesor asesor = new Asesor();
				asesor.setIdAsesor(rst.getInt("idtblBankingAdvisor"));
				asesor.setName(rst.getString("name"));
				asesor.setSpeciality(rst.getString("speciality"));
				listaAsesores.add(asesor);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return listaAsesores;
	}

	/**
	 * Guardado de Cliente
	 */
	@Override
	public int saveAsesor(Asesor client) {
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("INSERT INTO tblBankingAdvisor (name, speciality) VALUES (?, ?);");
			
			stmt.setString(1, client.getName());
			stmt.setString(2, client.getSpeciality());
			
			int res = stmt.executeUpdate();
			if(res==1) {
				Cache.getInstance().getAsesores(true);
			}
			return res;
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Editar cliente
	 */
	@Override
	public int editAsesor(Asesor asesorEdit) {
		try {
			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("UPDATE tblBankingAdvisor SET name = ?, speciality = ? WHERE idtblBankingAdvisor = ?");
			
			stmt.setString(1, asesorEdit.getName());
			stmt.setString(2, asesorEdit.getSpeciality());
			stmt.setInt(3, asesorEdit.getIdAsesor());
			
			int res = stmt.executeUpdate();
			if(res==1) {
				Cache.getInstance().getAsesores(true);
			}
			return res;
			
		}catch (JdbcSQLIntegrityConstraintViolationException e) {
			return -1;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Delete asesor
	 */
	@Override
	public int deleteAsesor(Asesor asesor) {
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("DELETE tblBankingAdvisor WHERE idtblBankingAdvisor = ?");
			
			stmt.setInt(1, asesor.getIdAsesor());
			
			int res = stmt.executeUpdate();
			if(res==1) {
				Cache.getInstance().getAsesores(true);
			}
			return res;
		
		}catch (JdbcSQLIntegrityConstraintViolationException e) {
			return -1;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
