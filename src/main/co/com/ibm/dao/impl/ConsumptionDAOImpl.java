package main.co.com.ibm.dao.impl;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;

import main.co.com.ibm.dao.ConsumptionDAO;
import main.co.com.ibm.dto.Cache;
import main.co.com.ibm.dto.Consumtion;
import main.co.com.ibm.utils.Conexion;

/**
 * Implementación para el guardado de Consumos en BD
 * @author deysespo
 *
 */
public class ConsumptionDAOImpl implements ConsumptionDAO, Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * Consumos por id de Tarjeta
	 */
	@Override
	public List<Consumtion> consumptionListByCard(int idCard) {
		List<Consumtion> consumptionList = new ArrayList<>();
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("SELECT * FROM tblConsumption WHERE tblCard_idtblCard = ?");

			stmt.setInt(1, idCard);

			ResultSet rst = stmt.executeQuery();
			while (rst.next()) {
				Consumtion consumption = new Consumtion();
				consumption.setIdConsumtion(rst.getInt("idtblConsumption"));
				consumption.setDate(rst.getString("date"));
				consumption.setDescription(rst.getString("description"));
				consumption.setAmount(rst.getFloat("amount"));
				consumptionList.add(consumption);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return consumptionList;
	}

	/**
	 * Guardar consumo
	 */
	@Override
	public int saveConsumption(Consumtion consumption) {
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("INSERT INTO tblConsumption (date, description, amount, tblCard_idtblCard) VALUES (?, ?, ?, ?);");
			
			stmt.setString(1, consumption.getDate());
			stmt.setString(2, consumption.getDescription());
			stmt.setFloat(3, consumption.getAmount());
			stmt.setInt(4, consumption.getIdCard());
			
			int res = stmt.executeUpdate();
			if(res==1) {
				Cache.getInstance().getClients(true);
			}
			return res;
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Eliminar consumo
	 */
	@Override
	public int deleteConsumption(Consumtion consumption) {
		try {

			PreparedStatement stmt = Conexion.getConexion()
					.prepareStatement("DELETE tblConsumption WHERE idtblConsumption = ?");
			
			stmt.setInt(1, consumption.getIdConsumtion());
			
			int res = stmt.executeUpdate();
			if(res==1) {
				Cache.getInstance().getClients(true);
			}
			return res;
		
		}catch (JdbcSQLIntegrityConstraintViolationException e) {
			return -1;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
