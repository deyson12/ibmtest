package main.co.com.ibm.dao;

import java.util.List;

import main.co.com.ibm.dto.Consumtion;

public interface ConsumptionDAO {

	List<Consumtion> consumptionListByCard(int idCard);

	int saveConsumption(Consumtion consumption);

	int deleteConsumption(Consumtion consumption);
	
}
