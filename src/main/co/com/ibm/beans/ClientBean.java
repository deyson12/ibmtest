package main.co.com.ibm.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import main.co.com.ibm.controller.AsesorController;
import main.co.com.ibm.controller.CardController;
import main.co.com.ibm.controller.ClientController;
import main.co.com.ibm.controller.ConsumptionController;
import main.co.com.ibm.dto.Asesor;
import main.co.com.ibm.dto.Cache;
import main.co.com.ibm.dto.Card;
import main.co.com.ibm.dto.Client;
import main.co.com.ibm.dto.Consumtion;
import main.co.com.ibm.utils.Util;

/**
 * Clase Bean para el Control de Eventos de la Vista
 * @author deysespo
 *
 */
@ManagedBean(name="clientBean")
@ViewScoped
public class ClientBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private ClientController clientController;
	private CardController cardController;
	private ConsumptionController consumptionController;
	private AsesorController asesorController;
	
	private Util util = new Util();
	
	private Card card;
	private Card cardEdit;
	
	private Client client;
	private Client clientEdit;
	
	private Asesor asesor;
	private Asesor asesorEdit;
	
	private Consumtion consumption;
	
	private String num1;
	private String num2;
	private String num3;
	private String num4;
	
	private String num1e;
	private String num2e;
	private String num3e;
	private String num4e;
	
	/**
	 * Inicialización de objetos del Bean
	 */
	@PostConstruct
	public void init(){
		card = new Card();
		cardEdit = new Card();
		client = new Client();
		clientEdit = new Client();
		consumption = new Consumtion();
		asesor = new Asesor();
		asesorEdit = new Asesor();
		
		num1 = "";
		num2 = "";
		num3 = "";
		num4 = "";
		
		num1e = "";
		num2e = "";
		num3e = "";
		num4e = "";
		
		clientController = new ClientController();
		cardController = new CardController();
		consumptionController = new ConsumptionController();
		asesorController = new AsesorController();
	}
	
	/**
	 * Metodo para guardar las tarjetas de un cliente
	 */
	public void saveCard() {
		card.setNumber(num1+"-"+num2+"-"+num3+"-"+num4);
		if(validateFieldsCard(card,num1,num2,num3,num4)) {
			int resultado = cardController.saveCard(card);
			
			if(resultado==1) {
				util.viewMessage(Util.OK, "Guardando Tarjeta",  "Exito");
				init();
			}else {
				util.viewMessage(Util.ERR, "Guardando Tarjeta",  "Error");
			}
			
		}
	}
	
	/**
	 * Metodo para guardar clientes
	 */
	public void saveClient() {
		if(validateFieldsClient(client)) {
			int resultado = clientController.saveClient(client);
			
			if(resultado==1) {
				util.viewMessage(Util.OK, "Guardando Cliente",  "Exito");
				init();
			}else {
				util.viewMessage(Util.ERR, "Guardando Cliente",  "Error");
			}
			
		}
	}
	
	/**
	 * metodo para guardar asesor
	 */
	public void saveAsesor() {
		if(validateFieldsAsesor(asesor)) {
			int resultado = asesorController.saveAsesor(asesor);
			
			if(resultado==1) {
				util.viewMessage(Util.OK, "Guardando Asesor",  "Exito");
				init();
			}else {
				util.viewMessage(Util.ERR, "Guardando Asesor",  "Error");
			}
			
		}
	}
	
	/**
	 * metodo para editar asesor
	 */
	public void editAsesor() {
		if(validateFieldsAsesor(asesorEdit)) {
			int resultado = asesorController.editAsesor(asesorEdit);
			
			if(resultado==1) {
				util.viewMessage(Util.OK, "Guardando Asesor",  "Exito");
				init();
			}else {
				util.viewMessage(Util.ERR, "Guardando Asesor",  "Error");
			}
			
		}
	}
	
	/**
	 * Metodo para eliminar un cliente
	 */
	public void deleteAsesor() {
		int resultado = asesorController.deleteAsesor(asesor);
		
		if(resultado==1) {
			util.viewMessage(Util.OK, "Eliminando asesor",  "Exito");
			init();
		}else if(resultado == -1){
			util.viewMessage(Util.ERR, "Eliminando asesor",  "El cliente tiene tarjetas asociadas");
		}else{
			util.viewMessage(Util.ERR, "Eliminando asesor",  "Error");
		}
	}
	
	private boolean validateFieldsAsesor(Asesor asesor2) {
		return true;
	}

	/**
	 * Metoodo para Editar clientes
	 */
	public void editClient() {
		if(validateFieldsClient(clientEdit)) {
			int resultado = clientController.editClient(clientEdit);
			
			if(resultado==1) {
				util.viewMessage(Util.OK, "Guardando Tarjeta",  "Exito");
				init();
			}else {
				util.viewMessage(Util.ERR, "Guardando Tarjeta",  "Error");
			}
			
		}
	}
	
	/**
	 * Metodo que permite en base a un cliente validar que los campos obligatoorios esten diligenciados
	 * @param client
	 * @return
	 */
	public boolean validateFieldsClient(Client client) {
		StringBuilder msg = new StringBuilder();
		if(client.getName().isEmpty()) {
			msg.append("<br> El campo Nombre es requerido");
		}
		if(client.getAddress().isEmpty()) {
			msg.append("<br> El campo Dirección es requerido");
		}
		if(client.getCity().isEmpty()) {
			msg.append("<br> El campo Ciudad es requerido");
		}
		if(client.getPhone().isEmpty()) {
			msg.append("<br> El campo Telefono es requerido");
		}else if(!Util.isNumber(client.getPhone())) {
			msg.append("<br> El campo Telefono debe ser númerico");
		}
		if(!msg.toString().isEmpty()) {
			util.viewMessage(Util.ERR, "Guardando Tarjeta",  msg.toString());
			return false;
		}
		return true;
	}

	/**
	 * Metodo que Permite validar los campos oobligatorios de una Tarjeta Antes de Guardar o Editar
	 * @param card2
	 * @param num1
	 * @param num2
	 * @param num3
	 * @param num4
	 * @return
	 */
	private boolean validateFieldsCard(Card card2, String num1, String num2, String num3, String num4) {
		StringBuilder msg = new StringBuilder();
		if(num1.length()!=4 || !Util.isNumber(num1) ||
		   num2.length()!=4 || !Util.isNumber(num2) ||
		   num3.length()!=4 || !Util.isNumber(num3) ||
		   num4.length()!=4 || !Util.isNumber(num4)) {
			msg.append("<br> Todos los campos dl número de tarjeta deben ser númericos de 4 digitos");
		}
		if("".equals(card2.getType())) {
			msg.append("<br> El campo tipo tarjeta es requerido");
		}
		if("".equals(card2.getCcv())) {
			msg.append("<br> El campo CCV es requerido");
		}else if(!Util.isNumber(card2.getCcv()) || card2.getCcv().length()<3) {
			msg.append("<br> El campo CCV debe ser númerico de 3 o 4 digitos");
		}
		
		if(!msg.toString().isEmpty()) {
			util.viewMessage(Util.ERR, "Guardando Tarjeta",  msg.toString());
			return false;
		}
		return true;
	}

	/**
	 * Metodo para editar una tarjeta
	 */
	public void editCard() {
		if(validateFieldsCard(cardEdit,num1e,num2e,num3e,num4e)) {
			cardEdit.setNumber(num1e+"-"+num2e+"-"+num3e+"-"+num4e);
			int resultado = cardController.editCard(cardEdit);
			
			if(resultado==1) {
				util.viewMessage(Util.OK, "Guardando Tarjeta",  "Exito");
				init();
			}else {
				util.viewMessage(Util.ERR, "Guardando Tarjeta",  "Error");
			}
			
		}
	}
	
	/**
	 * Metodo para eliminar una tarjeta
	 */
	public void deleteCard() {
		int resultado = cardController.deleteCard(card);
		
		if(resultado==1) {
			util.viewMessage(Util.OK, "Guardando Tarjeta",  "Exito");
			init();
		}else if(resultado == -1){
			util.viewMessage(Util.ERR, "Guardando Tarjeta",  "La tarjeta tiene historial de consumos");
		}else{
			util.viewMessage(Util.ERR, "Guardando Tarjeta",  "Error");
		}
		
	}
	
	/**
	 * Metodo para eliminar un cliente
	 */
	public void deleteClient() {
		int resultado = clientController.deleteClient(client);
		
		if(resultado==1) {
			util.viewMessage(Util.OK, "Guardando Tarjeta",  "Exito");
			init();
		}else if(resultado == -1){
			util.viewMessage(Util.ERR, "Guardando Tarjeta",  "El cliente tiene tarjetas asociadas");
		}else{
			util.viewMessage(Util.ERR, "Guardando Tarjeta",  "Error");
		}
	}
	
	/**
	 * Metodo para eliminar un consumo
	 */
	public void deleteConsumption() {
		int resultado = consumptionController.deleteClient(consumption);
		
		if(resultado==1) {
			util.viewMessage(Util.OK, "Guardando Tarjeta",  "Exito");
			init();
		}else if(resultado == -1){
			util.viewMessage(Util.ERR, "Guardando Tarjeta",  "El cliente tiene tarjetas asociadas");
		}else{
			util.viewMessage(Util.ERR, "Guardando Tarjeta",  "Error");
		}
	}
	
	/**
	 * Metodo para setear una tarjeta para editar
	 * @param cardEdit
	 */
	public void setCardEdit(Card cardEdit) {
		if(cardEdit!=null && !cardEdit.getNumber().isEmpty()) {
			String[] nums = cardEdit.getNumber().split("-");
			if(nums.length==4) {
				num1e = nums[0];
				num2e = nums[1];
				num3e = nums[2];
				num4e = nums[3];
			}
		}
		this.cardEdit = cardEdit;
	}

	/**
	 * Metodo que permite obtener los clientes en Cache
	 * @return
	 */
	public List<Client> getClientes() {
		return Cache.getInstance().getClients(false);
	}
	
	public List<Asesor> getAsesores() {
		return Cache.getInstance().getAsesores(false);
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public String getNum1() {
		return num1;
	}

	public void setNum1(String num1) {
		this.num1 = num1;
	}

	public String getNum2() {
		return num2;
	}

	public void setNum2(String num2) {
		this.num2 = num2;
	}

	public String getNum3() {
		return num3;
	}

	public void setNum3(String num3) {
		this.num3 = num3;
	}

	public String getNum4() {
		return num4;
	}

	public void setNum4(String num4) {
		this.num4 = num4;
	}

	public Card getCardEdit() {
		return cardEdit;
	}

	

	public String getNum1e() {
		return num1e;
	}

	public void setNum1e(String num1e) {
		this.num1e = num1e;
	}

	public String getNum2e() {
		return num2e;
	}

	public void setNum2e(String num2e) {
		this.num2e = num2e;
	}

	public String getNum3e() {
		return num3e;
	}

	public void setNum3e(String num3e) {
		this.num3e = num3e;
	}

	public String getNum4e() {
		return num4e;
	}

	public void setNum4e(String num4e) {
		this.num4e = num4e;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Client getClientEdit() {
		return clientEdit;
	}

	public void setClientEdit(Client clientEdit) {
		this.clientEdit = clientEdit;
	}

	public Consumtion getConsumption() {
		return consumption;
	}

	public void setConsumption(Consumtion consumption) {
		this.consumption = consumption;
	}

	public ClientController getClientController() {
		return clientController;
	}

	public void setClientController(ClientController clientController) {
		this.clientController = clientController;
	}

	public CardController getCardController() {
		return cardController;
	}

	public void setCardController(CardController cardController) {
		this.cardController = cardController;
	}

	public ConsumptionController getConsumptionController() {
		return consumptionController;
	}

	public void setConsumptionController(ConsumptionController consumptionController) {
		this.consumptionController = consumptionController;
	}

	public Util getUtil() {
		return util;
	}

	public void setUtil(Util util) {
		this.util = util;
	}

	public Asesor getAsesor() {
		return asesor;
	}

	public void setAsesor(Asesor asesor) {
		this.asesor = asesor;
	}

	public Asesor getAsesorEdit() {
		return asesorEdit;
	}

	public void setAsesorEdit(Asesor asesorEdit) {
		this.asesorEdit = asesorEdit;
	}
	
}
