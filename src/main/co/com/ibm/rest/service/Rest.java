package main.co.com.ibm.rest.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;

import main.co.com.ibm.controller.CardController;
import main.co.com.ibm.controller.ConsumptionController;
import main.co.com.ibm.dto.Card;
import main.co.com.ibm.dto.Consumtion;
import main.co.com.ibm.dto.rest.ConsumptionRequest;
import main.co.com.ibm.dto.rest.Response;
import main.co.com.ibm.utils.Util;

/**
 * Servicio REST para el registro de Consumos de una tarjeta
 * @author deysespo
 *
 */
@Path("/consultaCliente")
public class Rest {
	
	private CardController cardController = new CardController();
	private ConsumptionController consumptionController = new ConsumptionController();
	
	/**
	 * Guarda el Consumo
	 * @param request
	 * @return
	 */
	@POST
	public String saveConsumption(String request) {
        
		Gson gson = new Gson();
		Response res = new Response();
		
		ConsumptionRequest consumptionRequest = getConsumption(request, gson);
		
		if(consumptionRequest!=null) {
			Card card = cardController.getByCardNumber(consumptionRequest.getCardNumber());
			
			String validation = validateFields(consumptionRequest);
			
			if(validation.isEmpty()) {
				if(Util.isSimpleDate(consumptionRequest.getDate())) {
					if(card!=null) {
						int idCard = card.getIdCard();
						
						Consumtion consumption = new Consumtion();
						
						consumption.setIdCard(idCard);
						consumption.setDate(consumptionRequest.getDate());
						consumption.setDescription(consumptionRequest.getDescription());
						consumption.setAmount(consumptionRequest.getAmount());
						
						int save = consumptionController.saveConsumption(consumption);
						
						if(save==1) {
							res.setCode("IBM-OK-0");
							res.setMessage("El consumo se guardo exitosamente");
						}else {
							res.setCode("IBM-ERR-3");
							res.setMessage("No se actualiz� ningun registro");
						}
						
					}else {
						res.setCode("IBM-ERR-2");
						res.setMessage("El n�mero de la tarjeta no existe");
					}
				}else{
					res.setCode("IBM-ERR-4");
					res.setMessage("El formato de la fecha no es correcto");
				}
			}else{
				res.setCode("IBM-ERR-5");
				res.setMessage(validation);
			}
			
		}else{
			res.setCode("IBM-ERR-1");
			res.setMessage("El JSON de la petici�n est� mal armado");
		}
		
		return gson.toJson(res);
		
    }

	/**
	 * Valida que los campos del Request esten correctos
	 * @param consumptionRequest
	 * @return
	 */
	private String validateFields(ConsumptionRequest consumptionRequest) {
		StringBuilder msg = new StringBuilder();
		if(StringUtils.isBlank(consumptionRequest.getCardNumber()) || consumptionRequest.getCardNumber().isEmpty()) {
			msg.append("- cardNumber");
		}else if(StringUtils.isBlank(consumptionRequest.getDate()) || consumptionRequest.getDate().isEmpty()) {
			msg.append("- date");
		}else if(StringUtils.isBlank(consumptionRequest.getDescription()) || consumptionRequest.getDescription().isEmpty()) {
			msg.append("- description");
		}else if(consumptionRequest.getAmount()==0) {
			msg.append("- amount");
		}
		
		return msg.toString().isEmpty()?"":"Los campos: " + msg.toString() + ", son requeridos";
	}

	/**
	 * Valida que el JSON del consumo este correctamente armado
	 * @param request
	 * @param gson
	 * @return
	 */
	private ConsumptionRequest getConsumption(String request, Gson gson) {
		try {
			ConsumptionRequest consumptionRequest =  gson.fromJson(request, ConsumptionRequest.class);
			return consumptionRequest;
		}catch (Exception e) {
			return null;
		}
	}

	public void setCardController(CardController cardController) {
		this.cardController = cardController;
	}

	public void setConsumptionController(ConsumptionController consumptionController) {
		this.consumptionController = consumptionController;
	}	
}
