package main.co.com.ibm.spring.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;

import main.co.com.ibm.dao.ClientDAO;
import main.co.com.ibm.dto.Client;

/**
 * Servicio REST con Sprint para Listar un Cliente por ID
 * @author deysespo
 *
 */
@Controller
public class ClientRestController {

	@Autowired
	private Gson jsonTransformer;

	@Autowired
	ClientDAO clientDao;

	@RequestMapping(value = "/Client/{idCLient}", method = RequestMethod.GET, produces = "application/json")
	public void read(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			@PathVariable("idCLient") int idCLient) {

		try {

			Client client = clientDao.get(idCLient);
			String jsonSalida = jsonTransformer.toJson(client);

			httpServletResponse.getWriter().println(jsonSalida);
			httpServletResponse.setStatus(HttpServletResponse.SC_OK);
			httpServletResponse.setContentType("application/json; charset=UTF-8");

		} catch (IOException ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);

		} catch (Exception ex) {
			httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

		}
	}
	
	
}