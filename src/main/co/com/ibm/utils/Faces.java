package main.co.com.ibm.utils;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

/**
 * Clase para la impresión de Mensajes de JSF
 * @author deysespo
 *
 */
public class Faces {
	
	/**
	 * Imprime un mensaje en concreto
	 * @param severity
	 * @param title
	 * @param messageText
	 * @return
	 */
	public int viewMessage(int severity, String title, String messageText) {
		
		Severity severityFaces = severity==Util.OK?FacesMessage.SEVERITY_INFO:FacesMessage.SEVERITY_ERROR;
		
		FacesMessage message = new FacesMessage(severityFaces, title,  messageText);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return 1;
	}
	
}
