package main.co.com.ibm.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Clase Inical para cargar la BD o crearla en caso de no existir
 * @author deysespo
 *
 */
public class SessionListener implements ServletContextListener,
    HttpSessionListener {

    // Constante para el nombre de los ficheros
    private final static String DBFILENAME = "ibm";

    String url;
    Connection globalConnection;

    /**
     * Inicializamos el Contexto
     */
    public void contextInitialized(ServletContextEvent sce) {
        String spath = sce.getServletContext().getInitParameter("h2.database.path");
        if (spath == null) {
            spath = System.getProperty("user.home");
        }
        
        //Validamos si la BD existe
        boolean exists = new File(spath, DBFILENAME+".mv.db").exists();

        try {
            Class.forName("org.h2.Driver");
            File dbfile = new File(spath, DBFILENAME);
            url = "jdbc:h2:file:" + dbfile.getAbsolutePath().replaceAll("\\\\", "/");
            globalConnection = openConnection();
            Conexion.setConexion(globalConnection);
            
            // Si no existe, creamos la BD y llenamos los registros iniciales
            if (!exists) {
                initDb();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Connection openConnection() throws SQLException {
        return DriverManager.getConnection(url, "", "sa");
    }

    public void contextDestroyed(ServletContextEvent sce) {
        try {
            globalConnection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void sessionCreated(HttpSessionEvent se) {
        try {
            se.getSession().setAttribute("h2.connection", openConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void sessionDestroyed(HttpSessionEvent se) {
        try {
            Connection con = (Connection)se.getSession().getAttribute("h2.connection");
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Inicalizamos los datos de la BD e insertamos los registros de inicio
     * @throws SQLException
     * @throws IOException
     */
    private void initDb() throws SQLException, IOException {
        InputStream is = getClass().getClassLoader().getResourceAsStream("squema-h2.sql");
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        Connection connection = null;
        try {
            connection = openConnection();
            String line = br.readLine();
            StringBuilder statement = new StringBuilder();
            while (line != null) {
                line = line.trim();
                if (!line.startsWith("--") && !line.startsWith("#") && !line.startsWith("//")) {
                    statement.append(line);
                    if (line.endsWith(";")) {
                        executeLine(connection, statement.toString());
                        statement = new StringBuilder();
                    }
                }
                line = br.readLine();
            }
            if (statement.length() > 0) {
                executeLine(connection, statement.toString());
            }
        } finally {
            try {
                br.close();
            } catch (Exception e) {;}
            try {
                if (connection != null) connection.close();
            } catch (Exception e) {;}
        }
    }

    /**
     * Ejecutamos los Querys
     * @param connection
     * @param statement
     * @throws SQLException
     */
    private void executeLine(Connection connection, String statement) throws SQLException {
        PreparedStatement pstmt = connection.prepareStatement(statement);
        pstmt.execute();
        pstmt.close();
    }
}