package main.co.com.ibm.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Clase Utilitaria para validaciones y demas
 * @author deysespo
 *
 */
public class Util {
	
	public static int OK = 1;
	public static int ERR = 2;
	
	private Faces faces = new Faces();
	
	/**
	 * Validamos si un campo es Numerico
	 * @param number
	 * @return
	 */
	public static boolean isNumber(String number) {
		try {
			Float.parseFloat(number);
		}catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Validamos si un campo es Fecha
	 * @param date
	 * @return
	 */
	public static boolean isSimpleDate(String date) {
		try {
			String[] dateSplit = date.split("-");
			
			LocalDate today = LocalDate.of(Integer.parseInt(dateSplit[0]), Integer.parseInt(dateSplit[1]), Integer.parseInt(dateSplit[2]));
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	        formatter.format(today);
			
		}catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Mostramos un mensaje de JSF en Pantalla
	 * @param severity
	 * @param title
	 * @param messageText
	 * @return
	 */
	public int viewMessage(int severity, String title, String messageText) {
		return faces.viewMessage(severity, title, messageText);
	}
	
}
