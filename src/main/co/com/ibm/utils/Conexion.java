package main.co.com.ibm.utils;

import java.sql.Connection;

/**
 * Clase Singleton para la conexión
 * @author deysespo
 *
 */
public class Conexion {

	private static Connection con;

	public static void setConexion(Connection globalConnection) {
		if(con==null) {
			con = globalConnection;
		}
	}
	
	public static Connection getConexion() {
		return con;
	}
	
}
