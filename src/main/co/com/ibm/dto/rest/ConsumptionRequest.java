package main.co.com.ibm.dto.rest;

/**
 * Objeto Request para utilizar al momento de consumir el Servicio REST de Consumos
 * @author deysespo
 *
 */
public class ConsumptionRequest {

	private String cardNumber;
	private String date;
	private String description;
	private float amount;
	
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
}
