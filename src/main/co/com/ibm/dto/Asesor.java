package main.co.com.ibm.dto;

import java.io.Serializable;
import java.util.List;

/**
 * DTO para los clientes
 * @author deysespo
 *
 */
public class Asesor implements Serializable{

	private static final long serialVersionUID = 1L;
	private int idAsesor;
	private String name;
	private String speciality;
	
	public int getIdAsesor() {
		return idAsesor;
	}
	public void setIdAsesor(int idAsesor) {
		this.idAsesor = idAsesor;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSpeciality() {
		return speciality;
	}
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	
}
