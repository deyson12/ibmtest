package main.co.com.ibm.dto;

import java.io.Serializable;

/**
 * DTO de Consumos
 * @author deysespo
 *
 */
public class Consumtion implements Serializable{

	private static final long serialVersionUID = 1L;
	private int idConsumtion;
	private String date;
	private String description;
	private float amount;
	private int idCard;
	
	public int getIdConsumtion() {
		return idConsumtion;
	}
	public void setIdConsumtion(int idConsumtion) {
		this.idConsumtion = idConsumtion;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public int getIdCard() {
		return idCard;
	}
	public void setIdCard(int idCard) {
		this.idCard = idCard;
	}
	
}
