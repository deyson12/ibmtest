package main.co.com.ibm.dto;

import java.io.Serializable;
import java.util.List;

/**
 * DTO para las tarjetas
 * @author deysespo
 *
 */
public class Card implements Serializable{

	private static final long serialVersionUID = 1L;
	private int idCard;
	private String number;
	private String ccv;
	private String type;
	private List<Consumtion> consumptions;
	private int idClient;
	
	public int getIdCard() {
		return idCard;
	}
	public void setIdCard(int idCard) {
		this.idCard = idCard;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getCcv() {
		return ccv;
	}
	public void setCcv(String ccv) {
		this.ccv = ccv;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<Consumtion> getConsumptions() {
		return consumptions;
	}
	public void setConsumptions(List<Consumtion> consumptions) {
		this.consumptions = consumptions;
	}
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	
	 
}
