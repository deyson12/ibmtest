package main.co.com.ibm.dto;

import java.io.Serializable;
import java.util.List;

/**
 * DTO para los clientes
 * @author deysespo
 *
 */
public class Client implements Serializable{

	private static final long serialVersionUID = 1L;
	private int idClient;
	private String name;
	private String address;
	private String city;
	private String phone;
	private List<Card> cards;
	
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public List<Card> getCards() {
		return cards;
	}
	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
	
}
