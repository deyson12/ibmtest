package main.co.com.ibm.dto;

import java.util.List;

import main.co.com.ibm.dao.AsesorDAO;
import main.co.com.ibm.dao.ClientDAO;
import main.co.com.ibm.dao.impl.AsesorDAOImpl;
import main.co.com.ibm.dao.impl.ClientDAOImpl;

/**
 * Clase para el cache de la aplicación
 * @author deysespo
 *
 */
public class Cache {
	
	private static Cache cache = null;
	
	private ClientDAO clientDao = new ClientDAOImpl();
	private AsesorDAO asesorDao = new AsesorDAOImpl();
	
	private List<Client> clients;
	private List<Asesor> asesores;
	
	public static Cache getInstance(){
		if(cache==null){
			cache = new Cache();
		}
		return cache;
	}

	public List<Client> getClients(boolean clean) {
		if(this.clients==null || clean) {
			this.clients = clientDao.list();
		}
		return clients;
	}

	public List<Asesor> getAsesores(boolean clean) {
		if(this.asesores==null || clean) {
			this.asesores = asesorDao.list();
		}
		return asesores;
	}
}
