package main.co.com.ibm.controller;

import java.io.Serializable;

import main.co.com.ibm.dao.AsesorDAO;
import main.co.com.ibm.dao.impl.AsesorDAOImpl;
import main.co.com.ibm.dto.Asesor;
import main.co.com.ibm.dto.Client;

/**
 * Controlado para los clientes
 * @author deysespo
 *
 */
public class AsesorController implements Serializable{

	private static final long serialVersionUID = 1L;
	private AsesorDAO asesorDao = new AsesorDAOImpl();
	
	/**
	 * Guardado de Cliente
	 * @param client
	 * @return
	 */
	public int saveAsesor(Asesor asesor) {
		return asesorDao.saveAsesor(asesor);
	}

	/**
	 * Editar cliente
	 * @param clientEdit
	 * @return
	 */
	public int editAsesor(Asesor asesorEdit) {
		return asesorDao.editAsesor(asesorEdit);
	}
	
	/**
	 * Eliminar cliente
	 * @param client
	 * @return
	 */
	public int deleteAsesor(Asesor asesor) {
		return asesorDao.deleteAsesor(asesor);
	}
}
