package main.co.com.ibm.controller;

import java.io.Serializable;

import main.co.com.ibm.dao.ClientDAO;
import main.co.com.ibm.dao.impl.ClientDAOImpl;
import main.co.com.ibm.dto.Client;

/**
 * Controlado para los clientes
 * @author deysespo
 *
 */
public class ClientController implements Serializable{

	private static final long serialVersionUID = 1L;
	private ClientDAO clientDao = new ClientDAOImpl();
	
	/**
	 * Guardado de Cliente
	 * @param client
	 * @return
	 */
	public int saveClient(Client client) {
		return clientDao.saveClient(client);
	}

	/**
	 * Editar cliente
	 * @param clientEdit
	 * @return
	 */
	public int editClient(Client clientEdit) {
		return clientDao.editClient(clientEdit);
	}
	
	/**
	 * Eliminar cliente
	 * @param client
	 * @return
	 */
	public int deleteClient(Client client) {
		return clientDao.deleteClient(client);
	}
}
