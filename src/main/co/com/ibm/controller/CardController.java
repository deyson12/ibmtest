package main.co.com.ibm.controller;

import java.io.Serializable;

import main.co.com.ibm.dao.CardDAO;
import main.co.com.ibm.dao.impl.CardDAOImpl;
import main.co.com.ibm.dto.Card;

/**
 * Controlador para las tarjetas
 * @author deysespo
 *
 */
public class CardController implements Serializable{

	private static final long serialVersionUID = 1L;
	private CardDAO cardDao = new CardDAOImpl();
	
	/**
	 * Guardar tarjetas
	 * @param card
	 * @return
	 */
	public int saveCard(Card card) {
		return cardDao.saveCard(card);
	}

	/**
	 * Editar tarjetas
	 * @param cardEdit
	 * @return
	 */
	public int editCard(Card cardEdit) {
		return cardDao.editCard(cardEdit);
	}

	/**
	 * Eliminar tarjetas
	 * @param card
	 * @return
	 */
	public int deleteCard(Card card) {
		return cardDao.deleteCard(card);
	}
	
	/**
	 * Obtener tarjeta por numero de tarjeta
	 * @param cardNumber
	 * @return
	 */
	public Card getByCardNumber(String cardNumber) {
		return cardDao.getByCardNumber(cardNumber);
	}
}
