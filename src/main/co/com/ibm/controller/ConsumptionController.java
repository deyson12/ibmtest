package main.co.com.ibm.controller;

import java.io.Serializable;

import main.co.com.ibm.dao.ConsumptionDAO;
import main.co.com.ibm.dao.impl.ConsumptionDAOImpl;
import main.co.com.ibm.dto.Consumtion;

/**
 * Controlador para Consumos
 * @author deysespo
 *
 */
public class ConsumptionController implements Serializable{

	private static final long serialVersionUID = 1L;
	private ConsumptionDAO consumptionDao = new ConsumptionDAOImpl();

	/**
	 * Guardado de Consumos
	 * @param consumption
	 * @return
	 */
	public int saveConsumption(Consumtion consumption) {
		return consumptionDao.saveConsumption(consumption);
	}

	/**
	 * Eliminar consumo
	 * @param consumption
	 * @return
	 */
	public int deleteClient(Consumtion consumption) {
		return consumptionDao.deleteConsumption(consumption);
	}
}
