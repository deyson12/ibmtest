package test.co.com.ibm.rest.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import main.co.com.ibm.controller.CardController;
import main.co.com.ibm.controller.ConsumptionController;
import main.co.com.ibm.dto.Card;
import main.co.com.ibm.dto.Consumtion;
import main.co.com.ibm.rest.service.Rest;
import test.co.com.ibm.utils.MockUtils;

@RunWith(PowerMockRunner.class)
public class Rest_saveConsumption_Test {
	
	Rest spyRest;
	
	private static String JSON = "{\"cardNumber\": \"1234-4567-1234-0001\",\"date\": \"2019-04-16\",\"description\": \"Pago\",\"amount\": 3000.0,\"asd\":\"asd\"}";
	
	@Mock
	CardController cardControllerMock;
	
	@Mock
	ConsumptionController consumptionControllerMock;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		spyRest = Mockito.spy(new Rest());
		
		cardControllerMock = mock(CardController.class);
		consumptionControllerMock = mock(ConsumptionController.class);
		
		spyRest.setCardController(cardControllerMock);
		spyRest.setConsumptionController(consumptionControllerMock);
	}
	
	@Test
	public void saveConsumption(){
		
		Card card = MockUtils.getCard();
		
		when(cardControllerMock.getByCardNumber("1234-4567-1234-0001")).thenReturn(card);
		when(consumptionControllerMock.saveConsumption((Consumtion)Mockito.anyObject())).thenReturn(1);		
		
		String res = spyRest.saveConsumption(JSON);
		
		Assert.assertTrue(res.contains("IBM-OK-0"));
	}
	
	@Test
	public void saveConsumptionSaveError(){
		
		Card card = MockUtils.getCard();
		
		when(cardControllerMock.getByCardNumber("1234-4567-1234-0001")).thenReturn(card);	
		
		String res = spyRest.saveConsumption(JSON);
		
		Assert.assertTrue(res.contains("IBM-ERR-3"));
	}
	
	@Test
	public void saveConsumptionCardNotExist(){
		
		when(cardControllerMock.getByCardNumber("1234-4567-1234-0001")).thenReturn(null);	
		
		String res = spyRest.saveConsumption(JSON);
		
		Assert.assertTrue(res.contains("IBM-ERR-2"));
	}
	
	@Test
	public void saveConsumptionDateError(){
		
		Card card = MockUtils.getCard();
		
		when(cardControllerMock.getByCardNumber("1234-4567-1234-0001")).thenReturn(card);	
		
		String res = spyRest.saveConsumption(JSON.replace("2019-04-16", "2019-02-31"));
		
		Assert.assertTrue(res.contains("IBM-ERR-4"));
	}
	
	@Test
	public void saveConsumptionFieldsError(){
		
		Card card = MockUtils.getCard();
		
		when(cardControllerMock.getByCardNumber("1234-4567-1234-0001")).thenReturn(card);	
		
		String res = spyRest.saveConsumption(JSON.replace("2019-04-16", ""));
		
		Assert.assertTrue(res.contains("IBM-ERR-5"));
	}
	
	@Test
	public void saveConsumptionJsonError(){
		
		Card card = MockUtils.getCard();
		
		when(cardControllerMock.getByCardNumber("1234-4567-1234-0001")).thenReturn(card);	
		
		String res = spyRest.saveConsumption(JSON.replace("cardNumber", ""));
		
		Assert.assertTrue(res.contains("IBM-ERR-5"));
	}
	
	@Test
	public void saveConsumptionJsonMalformed(){
		
		Card card = MockUtils.getCard();
		
		when(cardControllerMock.getByCardNumber("1234-4567-1234-0001")).thenReturn(card);	
		
		String res = spyRest.saveConsumption(JSON.replace(",\"date\"", ""));
		
		Assert.assertTrue(res.contains("IBM-ERR-1"));
	}
}
