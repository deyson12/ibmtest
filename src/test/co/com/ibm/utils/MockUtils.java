package test.co.com.ibm.utils;

import main.co.com.ibm.dto.Card;
import main.co.com.ibm.dto.Client;

/**
 * Utilitario de objetos para los Mocks de las pruebas unitarias
 * @author deysespo
 *
 */
public class MockUtils {
	
	/**
	 * Mock de Tarjeta
	 * @return
	 */
	public static Card getCard() {
		Card card = new Card();
		card.setIdCard(1);
		card.setNumber("1111-2222-3333-4444");
		card.setType("Debito");
		card.setCcv("1234");
		card.setIdClient(1);
		
		return card;
	}
	
	/**
	 * Mock de Cliente
	 * @return
	 */
	public static Client getClient() {
		Client client = new Client();
		
		client.setIdClient(1);
		client.setName("Deyson");
		client.setAddress("Cll 47");
		client.setCity("Medell�n");
		client.setPhone("3136090247");
		
		return client;
	}
	
}
