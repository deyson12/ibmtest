package test.co.com.ibm.beans;

import static org.mockito.Mockito.mock;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import main.co.com.ibm.beans.ClientBean;
import main.co.com.ibm.dto.Client;
import main.co.com.ibm.utils.Util;
import test.co.com.ibm.utils.MockUtils;

/**
 * Clase para testear el metodo de validación de campos del cliente
 * @author deysespo
 *
 */
@RunWith(PowerMockRunner.class)
public class ClientBean_validateFieldsClient_Test {
	
	ClientBean spyClient;
	
	@Mock
	Util utilMock;
	
	/**
	 * Inicialización de los Mocks
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		spyClient = Mockito.spy(new ClientBean());
		
		utilMock = mock(Util.class);
		
		spyClient.setUtil(utilMock);
	}
	
	/**
	 * Este test sirve para validar los campos de un cliente
	 */
	@Test
	public void validateFieldsClient(){
		Client client = MockUtils.getClient();
		boolean validate = spyClient.validateFieldsClient(client);
		Assert.assertTrue(validate);
	}
	
	/**
	 * Este test sirve para validar si hay error en el Telefono
	 */
	@Test
	public void validateFieldsClientPhoneError(){
		Client client = MockUtils.getClient();
		client.setPhone("Telefono Malo");
		boolean validate = spyClient.validateFieldsClient(client);
		Assert.assertFalse(validate);
	}
	
}
