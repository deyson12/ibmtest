package test.co.com.ibm.beans;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import main.co.com.ibm.beans.ClientBean;
import main.co.com.ibm.controller.CardController;
import main.co.com.ibm.dto.Card;
import main.co.com.ibm.utils.Util;
import test.co.com.ibm.utils.MockUtils;

/**
 * Esta clase sirve para testear el guardado de una tarjeta
 * @author deysespo
 *
 */
@RunWith(PowerMockRunner.class)
public class ClientBean_saveCard_Test {
	
	ClientBean spyClient;
	
	@Mock
	CardController cardControllerMock;
	
	@Mock
	Util utilMock;
	
	/**
	 * Inicializamos los mocks
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		spyClient = Mockito.spy(new ClientBean());
		
		utilMock = mock(Util.class);
		cardControllerMock = mock(CardController.class);
		
		spyClient.setCardController(cardControllerMock);
		spyClient.setUtil(utilMock);
	}
	
	/**
	 * Sirve para probar el guardado de una tarjeta
	 */
	@Test
	public void saveCard(){
		
		spyClient.setNum1("1111");
		spyClient.setNum2("2222");
		spyClient.setNum3("3333");
		spyClient.setNum4("4444");
		
		Card card = MockUtils.getCard();
		
		spyClient.setCard(card);
		
		when(utilMock.viewMessage(Util.OK, "Guardando Tarjeta",  "Exito")).thenReturn(1);
		
		when(cardControllerMock.saveCard(card)).thenReturn(1);
		
		spyClient.saveCard();
		Mockito.verify(spyClient, Mockito.times(1)).init();
	}
	
	/**
	 * Sirve para validar error al guardar la tarjeta en la BD
	 */
	@Test
	public void saveCardError(){
		
		spyClient.setNum1("1111");
		spyClient.setNum2("2222");
		spyClient.setNum3("3333");
		spyClient.setNum4("4444");
		
		Card card = MockUtils.getCard();
		
		spyClient.setCard(card);
		
		when(utilMock.viewMessage(Util.OK, "Guardando Tarjeta",  "Exito")).thenReturn(1);
		
		when(cardControllerMock.saveCard(card)).thenReturn(2);
		
		spyClient.saveCard();
		Mockito.verify(spyClient, Mockito.times(0)).init();
	}
	
	/**
	 * Sirve para testear error al validar los campos de la tarjeta
	 */
	@Test
	public void saveCardNoValidate(){
		
		spyClient.setNum1("1111");
		spyClient.setNum2("2222");
		spyClient.setNum3("3333");
		spyClient.setNum4("4444");
		
		Card card = MockUtils.getCard();
		card.setCcv("");
		
		spyClient.setCard(card);
		
		when(utilMock.viewMessage(Util.OK, "Guardando Tarjeta",  "Exito")).thenReturn(1);
		
		when(cardControllerMock.saveCard(card)).thenReturn(2);
		
		spyClient.saveCard();
		Mockito.verify(cardControllerMock, Mockito.times(0)).saveCard(card);
	}
	
}
