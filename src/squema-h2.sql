-- -----------------------------------------------------
-- Table tblClient
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblClient (
  idtblClient INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  address VARCHAR(100) NULL,
  city VARCHAR(30) NOT NULL,
  phone BIGINT(20) NULL,
  PRIMARY KEY (idtblClient))
;

-- -----------------------------------------------------
-- Table tblCard
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblCard (
  idtblCard INT NOT NULL AUTO_INCREMENT,
  number VARCHAR(19) NOT NULL,
  ccv INT(4) NOT NULL,
  type VARCHAR(50) NOT NULL,
  tblClient_idtblClient INT NOT NULL,
  PRIMARY KEY (idtblCard),
  FOREIGN KEY(tblClient_idtblClient) REFERENCES tblClient(idtblClient))
;


-- -----------------------------------------------------
-- Table tblConsumption
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblConsumption (
  idtblConsumption INT NOT NULL AUTO_INCREMENT,
  date DATE NOT NULL,
  description VARCHAR(100) NOT NULL,
  amount BIGINT(12) NOT NULL,
  tblCard_idtblCard INT NOT NULL,
  PRIMARY KEY (idtblConsumption),
  FOREIGN KEY(tblCard_idtblCard) REFERENCES tblCard(idtblCard));


-- -----------------------------------------------------
-- Table tblBankingAdvisor
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS tblBankingAdvisor (
  idtblBankingAdvisor INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  speciality VARCHAR(55) NOT NULL,
  PRIMARY KEY (idtblBankingAdvisor));
  
  
INSERT INTO tblClient (idtblClient, name, address, city, phone) VALUES (1, 'Deyson Estrada Posada', 'Cll 47 B', 'Medellin', 3136090247);
INSERT INTO tblClient (idtblClient, name, address, city, phone) VALUES (2, 'Angee Xilena Matoma', 'Cll 56 B', 'Cali', 3007429343);

INSERT INTO tblCard (idtblCard, number, ccv, type, tblClient_idtblClient) VALUES (1, '1234-4567-1234-0001', 1234, 'Credito', 1);
INSERT INTO tblCard (idtblCard, number, ccv, type, tblClient_idtblClient) VALUES (2, '1234-4567-1234-0002', 5678, 'Debito', 1);
INSERT INTO tblCard (idtblCard, number, ccv, type, tblClient_idtblClient) VALUES (3, '1234-4567-1234-0003', 1234, 'E-Prepago', 2);

INSERT INTO tblConsumption (idtblConsumption, date, description, amount, tblCard_idtblCard) VALUES (1, '2012-05-05', 'Pago Online - Adidas', 300000, 1);

INSERT INTO tblBankingAdvisor (name, speciality) VALUES ('Asesor 1','Creditos');
INSERT INTO tblBankingAdvisor (name, speciality) VALUES ('Asesor 2','Hipotecas');